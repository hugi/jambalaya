package jambalaya.interfaces;

public interface DateTimeStamped extends DateTimeStampedCreation, DateTimeStampedModification {}