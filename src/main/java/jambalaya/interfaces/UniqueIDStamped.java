package jambalaya.interfaces;

public interface UniqueIDStamped {

	public String uniqueID();

	public void setUniqueID( String value );
}