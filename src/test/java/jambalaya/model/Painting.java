package jambalaya.model;

import jambalaya.interfaces.DateTimeStamped;
import jambalaya.interfaces.UniqueIDStamped;
import jambalaya.model.auto._Painting;

public class Painting extends _Painting implements DateTimeStamped, UniqueIDStamped {}