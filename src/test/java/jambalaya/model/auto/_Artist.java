package jambalaya.model.auto;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.time.LocalDateTime;
import java.util.List;

import org.apache.cayenne.BaseDataObject;
import org.apache.cayenne.exp.Property;

import jambalaya.model.Painting;

/**
 * Class _Artist was generated by Cayenne.
 * It is probably a good idea to avoid changing this class manually,
 * since it may be overwritten next time code is regenerated.
 * If you need to make any customizations, please use subclass.
 */
public abstract class _Artist extends BaseDataObject {

    private static final long serialVersionUID = 1L; 

    public static final String ID_PK_COLUMN = "id";

    public static final Property<LocalDateTime> CREATION_DATE = Property.create("creationDate", LocalDateTime.class);
    public static final Property<LocalDateTime> MODIFICATION_DATE = Property.create("modificationDate", LocalDateTime.class);
    public static final Property<String> NAME = Property.create("name", String.class);
    public static final Property<String> UNIQUE_ID = Property.create("uniqueID", String.class);
    public static final Property<List<Painting>> PAINTINGS = Property.create("paintings", List.class);

    protected LocalDateTime creationDate;
    protected LocalDateTime modificationDate;
    protected String name;
    protected String uniqueID;

    protected Object paintings;

    public void setCreationDate(LocalDateTime creationDate) {
        beforePropertyWrite("creationDate", this.creationDate, creationDate);
        this.creationDate = creationDate;
    }

    public LocalDateTime creationDate() {
        beforePropertyRead("creationDate");
        return this.creationDate;
    }

    public void setModificationDate(LocalDateTime modificationDate) {
        beforePropertyWrite("modificationDate", this.modificationDate, modificationDate);
        this.modificationDate = modificationDate;
    }

    public LocalDateTime modificationDate() {
        beforePropertyRead("modificationDate");
        return this.modificationDate;
    }

    public void setName(String name) {
        beforePropertyWrite("name", this.name, name);
        this.name = name;
    }

    public String name() {
        beforePropertyRead("name");
        return this.name;
    }

    public void setUniqueID(String uniqueID) {
        beforePropertyWrite("uniqueID", this.uniqueID, uniqueID);
        this.uniqueID = uniqueID;
    }

    public String uniqueID() {
        beforePropertyRead("uniqueID");
        return this.uniqueID;
    }

    public void addToPaintings(Painting obj) {
        addToManyTarget("paintings", obj, true);
    }

    public void removeFromPaintings(Painting obj) {
        removeToManyTarget("paintings", obj, true);
    }

    @SuppressWarnings("unchecked")
    public List<Painting> paintings() {
        return (List<Painting>)readProperty("paintings");
    }

    @Override
    public Object readPropertyDirectly(String propName) {
        if(propName == null) {
            throw new IllegalArgumentException();
        }

        switch(propName) {
            case "creationDate":
                return this.creationDate;
            case "modificationDate":
                return this.modificationDate;
            case "name":
                return this.name;
            case "uniqueID":
                return this.uniqueID;
            case "paintings":
                return this.paintings;
            default:
                return super.readPropertyDirectly(propName);
        }
    }

    @Override
    public void writePropertyDirectly(String propName, Object val) {
        if(propName == null) {
            throw new IllegalArgumentException();
        }

        switch (propName) {
            case "creationDate":
                this.creationDate = (LocalDateTime)val;
                break;
            case "modificationDate":
                this.modificationDate = (LocalDateTime)val;
                break;
            case "name":
                this.name = (String)val;
                break;
            case "uniqueID":
                this.uniqueID = (String)val;
                break;
            case "paintings":
                this.paintings = val;
                break;
            default:
                super.writePropertyDirectly(propName, val);
        }
    }

    private void writeObject(ObjectOutputStream out) throws IOException {
        writeSerialized(out);
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        readSerialized(in);
    }

    @Override
    protected void writeState(ObjectOutputStream out) throws IOException {
        super.writeState(out);
        out.writeObject(this.creationDate);
        out.writeObject(this.modificationDate);
        out.writeObject(this.name);
        out.writeObject(this.uniqueID);
        out.writeObject(this.paintings);
    }

    @Override
    protected void readState(ObjectInputStream in) throws IOException, ClassNotFoundException {
        super.readState(in);
        this.creationDate = (LocalDateTime)in.readObject();
        this.modificationDate = (LocalDateTime)in.readObject();
        this.name = (String)in.readObject();
        this.uniqueID = (String)in.readObject();
        this.paintings = in.readObject();
    }

}
